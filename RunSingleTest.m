function dataOut = RunSingleTest(testType,folderPath)
%% This function is called from the Pass_Fail application
% It contains the logic required to perform a single test by calling all 
% of the helper functions

% Function inputs:
% testType - If the test is simulated or using hardware
% filePath - The file path of the simulated file

% Function outputs:
% data - Structure containing all the outputs from the helper functions

%% Import Data and process it
if testType == "Physical"
    % Read sensor etc
    % To call function to write
    % files_ds = datastore(folderPath,"Type","tabulartext","NumHeaderLines",2,...
    %   "VariableNames",["Time","ChannelC"]);
elseif testType == "Simulated"
    % Import CSV file and run dummy test
    filesName = dir(fullfile(folderPath,"*.csv")).name;
    files_ds = datastore(filesName,"Type","tabulartext","NumHeaderLines",2,...
        "VariableNames",["Time","ChannelC"]);
    pause(0.25)
else
    error("Incorrect Test Type")
end

% Call the PreprocessData function for the datastore
preprocessedFiles_ds = transform(files_ds,@PreprocessData);

%% Extract features of the signal 
ExtractedFeatures_ds = transform(preprocessedFiles_ds,@ExtractFeature);

dataOut = read(ExtractedFeatures_ds);
end
