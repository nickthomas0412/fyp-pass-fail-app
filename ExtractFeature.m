function dataOut = ExtractFeature(data)
%% This function extracts features from the input data

% Function inputs:
% data - data from the preprocessed stage

% Function outputs:
% dataOut - Structure containing the result of preprocessed stage and the
% extracted features

%% Copy previous structure
dataOut = data;

%% Identify peaks and therefore natural frequency modes

locs = islocalmax(data.amp,"MinProminence",0.0025);
dataOut.fPeaks = data.f(locs);
%plot(frequencyPeaks,P1(locs),'r^','markerfacecolor',[1 0 0])

end