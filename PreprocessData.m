function dataOut = PreprocessData(data)
%% This function preprocesses the input data
% 1 - cleans the data with low and high pass filters
% 2 - performs an FFT to extract the amplitude and phase against frequency
% 3 - 

% Function inputs:
% data - timeseries data from the sensor

% Function outputs:
% dataOut - Structure containing the result of the FFT

%% Clean the signal
dt = milliseconds((data.Time(2) - data.Time(1)));
fs = 1/seconds(dt);

data.ChannelC = lowpass(data.ChannelC,10000,fs);
data.ChannelC = highpass(data.ChannelC,50,fs);

dataOut.rawSig = data.ChannelC;
dataOut.rawSigTime = data.Time;

%% Perform the FFT
y = data.ChannelC;
NFFT = length(y);
Y = fft(y)/NFFT; % N-point complex DFT

dataOut.f = fs/NFFT*(0:(NFFT/2))';

%% Calculate the amplitude
P2 = abs(Y);
P2c = Y.*conj(Y)/NFFT;
P1 = P2(1:NFFT/2+1);
P1(2:end-1) = 2*P1(2:end-1);

dataOut.amp = P1;

%% Calculate the phase
Ymodified = Y;
threshold = max(P1)/3;
Ymodified(abs(Y) < threshold) = 0;

phase2 = atan2(imag(Ymodified),real(Ymodified));
phase1 = phase2(1:NFFT/2+1);

dataOut.phase = phase1;

end
